package base.test;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import pom.GeneralPage;
import wrapper.LogWrapper;

public class BaseTestWithFailureLogin extends BaseTest {
	private static final Logger logger = LogWrapper.createLogger(BaseTestWithFailureLogin.class.getName());

	@AfterMethod
	public void afterMethodWithFailureLogin() {
		logger.info("Accept Error Message Popup");
		GeneralPage general_page = new GeneralPage();
		general_page.acceptPopup();
	}
}
