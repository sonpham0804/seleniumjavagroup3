package base.test;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import actions.Actions;
import utilities.Utilities;
import wrapper.DriverConfig;
import wrapper.LogWrapper;
import wrapper.SortAssertionWrapper;
import wrapper.WebDriverWrapper;

public class BaseTest {
	private static final Logger logger = LogWrapper.createLogger(BaseTest.class.getName());
	public String strRandomString;
	public SortAssertionWrapper softAssertion;
	public Actions action;

	@BeforeMethod
	@Parameters({ "type", "browser" })
	public void beforeMethod(String type, String browser) {
		logger.info("Pre-condition");
		WebDriverWrapper.startDriver(type, browser);
		WebDriverWrapper.navigateTo(DriverConfig.getProperty(DriverConfig.KEY_STARTUP_URL));
		// Create a random string
		strRandomString = Utilities.createRandomString();
		// Create sort assert
		softAssertion = new SortAssertionWrapper();
		// Create action
		action = new Actions();
	}

	@AfterMethod
	public void afterMethod() {
		logger.info("Post-condition");
		logger.info("Close browser");
		WebDriverWrapper.quit();
	}
}
