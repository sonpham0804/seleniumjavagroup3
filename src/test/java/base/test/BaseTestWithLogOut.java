package base.test;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import pom.MainPage;
import wrapper.LogWrapper;

public class BaseTestWithLogOut extends BaseTest {
	private static final Logger logger = LogWrapper.createLogger(BaseTestWithLogOut.class.getName());

	@AfterMethod
	public void afterMethodWithLogOut() {
		logger.info("Post-condition");

		logger.info("Log Out from HomePage");
		MainPage main_page = new MainPage();
		main_page.logOut();

	}
}
