package da.login.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTest;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.User;
import pom.LogInPage;
import pom.MainPage;
import popup.BasePopup;
import wrapper.LogWrapper;

public class DA_LOGIN_TC005 extends BaseTest  {
	
	@Test(description = "Verify that there is no Login dialog when switching between 2 repositories with the same account")
	public void DA_LOGIN_TC005_Verify_that_there_is_no_Login_dialog_when_switching_between_2_repositories_with_the_same_account() {
		ExtentTestManager.startTest("DA_MP_TC005", "Verify that there is no Login dialog when switching between 2 repositories with the same account");
		
		User user = new User(Constant.ROOT_USER.getUserName(),"","SampleRepository");
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Login with valid account for the first repository");
		MainPage main_page = login_page.login(user);
		
		LogWrapper.report("Step3:Choose another repository in Repository list");
		main_page.chooseRepository(Constant.REPO);
		
		LogWrapper.report("VP:Observe the current page: There is no Login Repository dialog");
		BasePopup base_popup = new BasePopup();
		Boolean isPopupDisplay = base_popup.isPopupExist("Login Repository");
		softAssertion.sortAssertFalse(isPopupDisplay, "Login Repository dialog is displayed");
		
		LogWrapper.report("VP:Observe the current page:The Repository menu displays name of switched repository");
		String currentRepository = main_page.getCurrentRepositoryName();
		softAssertion.sortAssertEquals(currentRepository, Constant.REPO, "The Repository menu does not display name of switched repository");
	
		softAssertion.sortAssertAll();
	}
}