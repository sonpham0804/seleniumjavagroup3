package da.login.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTestWithFailureLogin;
import extent.report.ExtentTestManager;
import objects.User;
import pom.LogInPage;
import wrapper.LogWrapper;

public class DA_LOGIN_TC010 extends BaseTestWithFailureLogin  {
	
	@Test(description = "Verify that the page works correctly for the case when no input entered to Password and Username field")
	public void DA_LOGIN_TC010_Verify_that_the_page_works_correctly_for_the_case_when_no_input_entered_to_Password_and_Username_fiel() {
		ExtentTestManager.startTest("DA_LOGIN_TC010","Verify that the page works correctly for the case when no input entered to Password and Username field");
		
		String expected_msg = "Please enter username!";
		User invalidUser = new User("","");
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Click Login button without entering data into Username and Password field");
		login_page.failureLogin(invalidUser);
		
		LogWrapper.report("Step3:Observe the current page");
		String actual = login_page.getPopupText();
		
		LogWrapper.report("Verify that Dashboard Error message \"Username or password is invalid\" appears");
		softAssertion.sortAssertEquals(actual, expected_msg, "Dashboard Error message \\\"Please enter username!\\\" does not appears");

		softAssertion.sortAssertAll();
	}

}
