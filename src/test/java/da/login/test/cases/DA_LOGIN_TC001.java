package da.login.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTest;
import constant.Constant;
import extent.report.ExtentTestManager;
import pom.LogInPage;
import wrapper.LogWrapper;

public class DA_LOGIN_TC001 extends BaseTest  {
	
	@Test(description = "Verify that user can login specific repository successfully via Dashboard login page with correct credentials")
	public void DA_LOGIN_TC001_Verify_that_user_can_login_specific_repository_successfully_via_Dashboard_login_page_with_correct_credentials() {
		ExtentTestManager.startTest("DA_LOGIN_TC001", "Verify that user can login specific repository successfully via Dashboard login page with correct credentials");
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();	
		
		LogWrapper.report("Step2:Enter valid username and password");
		LogWrapper.report("Step3:Click on \"Login\" button");
		login_page.login();
		
		LogWrapper.report("VP:Verify that Dashboard Mainpage appears");
		softAssertion.sortAssertEquals(login_page.getTitle(), Constant.MAIN_TITLE,"Verify that Dashboard Mainpage appears");
		
		softAssertion.sortAssertAll();
	}

}
