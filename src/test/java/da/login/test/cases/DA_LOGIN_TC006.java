package da.login.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTestWithFailureLogin;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.User;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_LOGIN_TC006 extends BaseTestWithFailureLogin  {
	
	@Test(description = "Verify that Password input is case sensitive")
	public void DA_LOGIN_TC006_Verify_that_Password_input_is_case_sensitive() {
		ExtentTestManager.startTest("DA_LOGIN_TC006", "Verify that Password input is case sensitive");
		
		User invalidUser = new User(Constant.TEST_USER1.getUserName(),Constant.TEST_USER1.getPassword().toLowerCase());
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();	
		
		LogWrapper.report("Step2:Login with the account has uppercase password");
		MainPage main_page = login_page.login(Constant.TEST_USER1);

		LogWrapper.report("VP: Observe the current page: Main page is displayed");
		softAssertion.sortAssertEquals(main_page.getTitle(), Constant.MAIN_TITLE,"Verify that Main page is displayed");
		
		LogWrapper.report("Step3:Logout TA Dashboard");
		LogWrapper.report("Step4:Login with the above account but enter lowercase password");
		main_page.logOut(invalidUser.getUserName())
				.failureLogin(invalidUser);
		
		LogWrapper.report("VP: Observe the current page: Dashboard Error message \"Username or password is invalid\" appears");
		softAssertion.sortAssertEquals(login_page.getPopupText(), Constant.INVALID_USER_ERROR_MESSAGE,"Dashboard Error message \\\"Username or password is invalid\\\" appears");
	
		softAssertion.sortAssertAll();
	}

}
