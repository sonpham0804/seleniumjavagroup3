package da.login.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTestWithFailureLogin;
import extent.report.ExtentTestManager;
import objects.User;
import pom.LogInPage;
import wrapper.LogWrapper;

public class DA_LOGIN_TC003 extends BaseTestWithFailureLogin  {
	
	@Test(description = "Verify that user fails to log in specific repository successfully via Dashboard login page with correct username and incorrect password")
	public void DA_LOGIN_TC003_Verify_that_user_fails_to_log_in_specific_repository_successfully_via_Dashboard_login_page_with_correct_username_and_incorrect_password() {
		ExtentTestManager.startTest("DA_LOGIN_TC003", "Verify that user fails to log in specific repository successfully via Dashboard login page with correct username and incorrect password");
		
		String expected_msg = "Username or password is invalid";
		User invalidUser = new User("administrator","abc");
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Enter valid username and invalid password");
		login_page.failureLogin(invalidUser);
		
		LogWrapper.report("Step3:Click on \"Login\" button");
		String actual = login_page.getPopupText();
		
		LogWrapper.report("Verify that Dashboard Error message \"Username or password is invalid\" appears");
		softAssertion.sortAssertEquals(actual, expected_msg, "Username or password is invalid message does not appear");

		softAssertion.sortAssertAll();
	}

}
