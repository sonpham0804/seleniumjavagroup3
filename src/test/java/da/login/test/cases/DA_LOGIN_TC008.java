package da.login.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTest;
import constant.Constant;
import extent.report.ExtentTestManager;
import pom.LogInPage;
import wrapper.LogWrapper;

public class DA_LOGIN_TC008 extends BaseTest  {
	
	@Test(description = "Verify that password with special characters is working correctly")
	public void DA_LOGIN_TC008_Verify_that_password_with_special_characters_is_working_correctly() {
		ExtentTestManager.startTest("DA_LOGIN_TC008", "Verify that password with special characters is working correctly");
		
		LogWrapper.report("Step1: Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();	
		
		LogWrapper.report("Step2:Login with account that has special characters password");
		login_page.login(Constant.TEST_USER2);
		
		LogWrapper.report("VP:Observe the current page: Main page is displayed");
		softAssertion.sortAssertEquals(login_page.getTitle(), Constant.MAIN_TITLE,"Verify that Main page is displayed");
	
		softAssertion.sortAssertAll();
	}

}
