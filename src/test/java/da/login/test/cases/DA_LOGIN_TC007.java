package da.login.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTest;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.User;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_LOGIN_TC007 extends BaseTest  {
		
	@Test(description = "Verify that \"Username\" is not case sensitive")
	public void DA_LOGIN_TC007_Verify_that_Username_is_not_case_sensitive() {
		ExtentTestManager.startTest("DA_LOGIN_TC007", "Verify that \"Username\" is not case sensitive");
		
		User user = new User(Constant.ROOT_USER.getUserName().toUpperCase(),"","SampleRepository");
		User user2 = new User(Constant.ROOT_USER.getUserName(),"","SampleRepository");
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Login with the account has uppercase username\r\n");
		MainPage main_page = login_page.login(user);
		
		LogWrapper.report("Step3:Observe the current page\r\n");
		String actual = main_page.getTitle();
		softAssertion.sortAssertEquals(actual, Constant.MAIN_TITLE,"Verify that Dashboard Mainpage appears");
		
		LogWrapper.report("step4:Logout TA Dashboard");
		login_page = main_page.logOut();
		
		LogWrapper.report("step5:Login with the above account but enter lowercase username");
		main_page = login_page.login(user2);
		
		LogWrapper.report("step6:Observe the current page - Main page is displayed\r\n");
		String actual2 = main_page.getTitle();
		softAssertion.sortAssertEquals(actual2, Constant.MAIN_TITLE,"Dashboard Mainpage appears does not appear");
		
		softAssertion.sortAssertAll();
	}

}
