package da.login.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTestWithLogOut;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.User;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_LOGIN_TC004 extends BaseTestWithLogOut  {
	
	
	@Test(description = "Verify that user is able to log in different repositories successfully after logging out current repository")
	public void DA_LOGIN_TC004_Verify_that_user_is_able_to_log_in_different_repositories_successfully_after_logging_out_current_repository() {
		ExtentTestManager.startTest("DA_MP_TC004", "Verify that user is able to log in different repositories successfully after logging out current repository");
		
		User user = new User(Constant.ROOT_USER.getUserName(),"","SampleRepository");
		User user2 = new User(Constant.ROOT_USER.getUserName(),"",Constant.REPO);
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Enter valid username and password of default repository");
		LogWrapper.report("Step3:Click on \"Login\" button");
		MainPage main_page = login_page.login(user);
		
		LogWrapper.report("Click on \"Logout\" button");
		login_page = main_page.logOut();
		
		LogWrapper.report("Select a different repository");
		LogWrapper.report("Enter valid username and password of this repository");
		login_page.login(user2);
	
		LogWrapper.report("Verify that Dashboard Mainpage appears\r\n");
		String actual = login_page.getTitle();
		softAssertion.sortAssertEquals(actual, Constant.MAIN_TITLE,"Verify that Dashboard Mainpage appears");
		
		softAssertion.sortAssertAll();
	}

}
