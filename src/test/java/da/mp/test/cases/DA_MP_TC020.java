package da.mp.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTest;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.Page;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC020 extends BaseTest{
	
	@Test (description = "Verify that user is able to delete sibbling page as long as that page has not children page under it")
	public void DA_LOGIN_TC020_Verify_that_user_is_able_to_delete_sibbling_page_as_long_as_that_page_has_not_children_page_under_it() {
		ExtentTestManager.startTest("DA_MP_TC020", "Verify that user is able to delete sibbling page as long as that page has not children page under it");
		Page Overview = new Page("Overview",null,"","",true);
		Page page1 = new Page("Page 1" + strRandomString,Overview,"","",true);
		Page page2 = new Page("Page 2" + strRandomString,page1,"","",true);
		String page2_path = String.format("%s->%s->%s", "Overview",page1.getPageName(),page2.getPageName());
		String unable_delete_page_error_message = String.format(Constant.UNABLE_DELETE_PAGE_ERROR_MESSAGE, page1.getPageName());
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Log in specific repository with valid account");
		MainPage main_page = login_page.login();
		
		LogWrapper.report("Step3:Go to Global Setting -> Add page");
		LogWrapper.report("Step4:Enter info into all required fields on New Page dialog");
		action.addNewPage(page1);
		
		LogWrapper.report("Step5:Go to Global Setting -> Add page");
		LogWrapper.report("Step6:Enter info into all required fields on New Page dialog");
		action.addNewPage(page2);
		
		LogWrapper.report("Step7:Go to the first created page");
		main_page.gotoPage(page1);
		
		LogWrapper.report("Step8:Click Delete link");
		main_page.selectGlobalSettingMenu("Delete");
		
		LogWrapper.report("Step9:Click Ok button on Confirmation Delete page");
		main_page.acceptPopup();
		
		LogWrapper.report("VP:Check warning message \"Can not delete page 'Page 1' since it has children page(s)\" appears");
		softAssertion.sortAssertEquals(main_page.getPopupText(), unable_delete_page_error_message,"test");
		
		LogWrapper.report("Step11:Close confirmation dialog");
		main_page.acceptPopup();
		
		LogWrapper.report("Step12:Go to the second page");
		LogWrapper.report("Step13:Click Delete link");
		LogWrapper.report("Step14:Click Ok button on Confirmation Delete page");
		main_page.deletePage(page2);
		
		LogWrapper.report("VP:Page 2 is deleted successfully");
		boolean page2_exist = main_page.doesPageExist(page2_path);
		softAssertion.sortAssertFalse(page2_exist,page2_path + " still exists");
		
		softAssertion.sortAssertAll();
	}

}
