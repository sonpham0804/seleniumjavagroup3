package da.mp.test.cases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import base.test.BaseTestWithLogOut;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.Page;
import objects.User;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC013 extends BaseTestWithLogOut{
	private MainPage main_page;
	private LogInPage login_page;
	private Page page1;
	private Page page2;
	
	@Test(description = "Verify that the newly added main parent page is positioned at the location specified as set with \"Displayed After\" field of \"New Page\" form on the main page bar/\"Parent Page\" dropped down menu")
	public void DA_MP_TC013_Verify_that_the_newly_added_main_parent_page_is_positioned() {
		ExtentTestManager.startTest("DA_MP_TC013", "Verify that the newly added main parent page is positioned at the location specified as set with \"Displayed After\" field of \"New Page\" form on the main page bar/\"Parent Page\" dropped down menu");

		LogWrapper.report("Step1:Navigate to Dashboard login page");
		
		User user = new User(Constant.ROOT_USER.getUserName(),Constant.ROOT_USER.getPassword(),"SampleRepository");
		page1 = new Page("Page 1 " + strRandomString,null,"","",true);
		page2 = new Page("Page 2 " + strRandomString,null,"",page1.getPageName(),true);
		
		LogWrapper.report("Step2:Log in specific repository with valid account\r\n");
		login_page = new LogInPage();
		main_page = login_page.login(user);
		
		LogWrapper.report("Step3:Go to Global Setting -> Add page\r\n");	
		LogWrapper.report("Step4:Enter Page Name field\r\n");
		LogWrapper.report("Step5:Click OK button\r\n");
		
		
		action.addNewPage(page1);
		
		LogWrapper.report("Step6:Go to Global Setting -> Add page\r\n");
		LogWrapper.report("Step7:Enter Page Name field");
		LogWrapper.report("Step8:Click on Displayed After dropdown list");
		LogWrapper.report("Step9:Select specific page 1");
		LogWrapper.report("Step10:Click OK button");
		action.addNewPage(page2);
		
		LogWrapper.report("Step11:Check \"Another Test\" page is positioned besides the \"Test\" page");
		LogWrapper.report("Page 2 is positioned besides the Page 1");
		softAssertion.sortAssertEquals(main_page.getPageIndex(page1.getPageName()), 2,"The pages are not besided each other");
		softAssertion.sortAssertEquals(main_page.getPageIndex(page2.getPageName()), 3,"The pages are not besided each other");
		
		softAssertion.sortAssertAll();
	}
	
	@AfterMethod
	public void afterMethodforTC013() {
		main_page.deletePage(page1);
		main_page.deletePage(page2);
	}
}
