package da.mp.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTestWithLogOut;
import extent.report.ExtentTestManager;
import objects.Page;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC019 extends BaseTestWithLogOut{
	
	@Test(description = "Verify that user is able to add additional sibbling page levels to the parent page successfully")
	public void DA_MP_TC019_Verify_that_user_is_able_to_add_additional_sibbling_page_levels_to_the_parent_page_successfully() {
		ExtentTestManager.startTest("DA_MP_TC019", "Verify that user is able to add additional sibbling page levels to the parent page successfully");
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		Page Overview = new Page("Overview",null,"","",true);
		Page page1 = new Page("Page 1" + strRandomString,Overview,"","",true);
	
		LogWrapper.report("Step2:Log in specific repository with valid account\r\n");
		LogInPage login_page = new LogInPage();
		MainPage main_page = login_page.login();
		
		LogWrapper.report("Step3:Go to Global Setting -> Add page\r\n");	
		LogWrapper.report("Step4:Enter info into all required fields on New Page dialog");
		LogWrapper.report("Page name: Page 1 - Parent page: Overview");
		action.addNewPage(page1);
		
		LogWrapper.report("Step5:Observe the current page");
		LogWrapper.report("User is able to add additional sibbling page levels to parent page successfully. In this case: Overview is parent page of page 1");

		softAssertion.sortAssertTrue(main_page.doesPageExist("Overview->Page 1"), "Page does not exist under parent page");	
		
		main_page.deletePage(page1);
		softAssertion.sortAssertAll();
	}

}
