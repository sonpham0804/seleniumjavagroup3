package da.mp.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTestWithLogOut;
import extent.report.ExtentTestManager;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC014 extends BaseTestWithLogOut{
	
	@Test(description = "DA_MP_TC014: Verify Public pages can be visibale and accessed by all users of working repository")
	public void DA_LOGIN_TC014_Verify_Public_pages_can_be_visibale_and_accessed_by_all_users_of_working_repository() {
		ExtentTestManager.startTest("DA_LOGIN_TC014", "Verify Public pages can be visibale and accessed by all users of working repository");		
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Log in with valid account");
		MainPage main_page = login_page.login();
		
		LogWrapper.report("Step3:Go to Global Setting -> Add page");
		main_page.selectGlobalSettingMenu("Add Page");
		
		boolean clickable = main_page.isGlobalSettingMenuClickable();
		LogWrapper.report("VP:User can not to go to Global Setting -> Add page");
		softAssertion.sortAssertFalse(clickable,"Able to select Global Setting Menu");
		
		softAssertion.sortAssertAll();
	}

}
