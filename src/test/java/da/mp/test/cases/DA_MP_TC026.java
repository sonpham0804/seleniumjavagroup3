package da.mp.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTest;
import extent.report.ExtentTestManager;
import objects.Page;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC026 extends BaseTest{
	
	@Test (description = "Verify that page column is correct when user edit \"Number of Columns\" field of a specific page")
	public void DA_LOGIN_TC026_Verify_that_page_column_is_correct_when_user_edit_Number_of_Columns_field_of_a_specific_page() {
		ExtentTestManager.startTest("DA_MP_TC026", "Verify that page column is correct when user edit \"Number of Columns\" field of a specific page");
		
		Page page1 = new Page("Page 1" + strRandomString,null,"2","",false);
		Page page2 = new Page("",null,"3","",false);
		int expected_column_numbers = 3;
		
		LogWrapper.report("Step1: Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2: Login with valid account");
		MainPage main_page = login_page.login();
		
		LogWrapper.report("Step3: Go to Global Setting -> Add page");
		LogWrapper.report("Step4: Enter info into all required fields on New Page dialog");
		action.addNewPage(page1);
		
		LogWrapper.report("Step5: Go to Global Setting -> Edit link");
		LogWrapper.report("Step6: Edit Number of Columns for the above created page");
		LogWrapper.report("Step7: Click Ok button on Edit Page dialog");
		action.editPage(page1,page2);
		
		LogWrapper.report("VP: Observe the current page - There are 3 columns on the above created page");
		int coulumns_number = main_page.countColumns();
		softAssertion.sortAssertEquals(coulumns_number, expected_column_numbers,  "Incorect columns number!");
		
		softAssertion.sortAssertAll();
	}

}
