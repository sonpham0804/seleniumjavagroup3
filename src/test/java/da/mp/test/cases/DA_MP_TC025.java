package da.mp.test.cases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import base.test.BaseTestWithLogOut;
import extent.report.ExtentTestManager;
import objects.Page;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC025 extends BaseTestWithLogOut{
	private Page page1;
	private Page page2;
	private Page page2_edit;
	private LogInPage login_page;
	private MainPage main_page;
	
	@Test(description = "Verify that page listing is correct when user edit \"Display After\"  field of a specific page")
	public void DA_MP_TC025_Verify_that_page_listing_is_correct_when_user_edit_Display_After__field_of_a_specific_page() {
		ExtentTestManager.startTest("DA_MP_TC025", "Verify that page listing is correct when user edit \\\"Display After\\\"  field of a specific page");
		page1 = new Page("Page1 " + strRandomString,null,"","",true);
		page2 = new Page("Page2 " + strRandomString,null,"","",true);
		page2_edit = new Page("",null,"","Overview",true);
		int expected_index = 2;
		int expected_index2 = 3;
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogWrapper.report("Step2:Log in specific repository with valid account\r\n");
		login_page = new LogInPage();
		main_page = login_page.login();
		
		LogWrapper.report("Step3:Add a new page");
		LogWrapper.report("Step4:Enter info into all required fields on New Page dialog");
		action.addNewPage(page1);
		
		LogWrapper.report("Step5:Go to Global Setting -> Add page");	
		LogWrapper.report("Step6:Enter info into all required fields on New Page dialog");
		action.addNewPage(page2);
		
		LogWrapper.report("Step7:Click Edit link for the second created page");
		LogWrapper.report("Step8:Change value Display After for the second created page to after Overview page");
		LogWrapper.report("Step9:Click OK button");
		action.editPage(page1,page2_edit);
		
		LogWrapper.report("Step10:Observe the current page - Position of the second page follow Overview page");
		int index = main_page.getPageIndex(page1.getPageName());
		int index2 = main_page.getPageIndex(page2.getPageName());
		
		softAssertion.sortAssertEquals(index, expected_index,"Page 1 is not in correct place");
		softAssertion.sortAssertEquals(index2, expected_index2,"Page 2 is not in correct place");
		
		softAssertion.sortAssertAll();
	}
	@AfterMethod
	public void afterMethodforTC025() {
		main_page.deletePage(page1);
		main_page.deletePage(page2);
	}
}
