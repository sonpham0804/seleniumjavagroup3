package da.mp.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTest;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.Page;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC017 extends BaseTest{
	
	@Test (description = "Verify that user can remove any main parent page except \"Overview\" page successfully and the order of pages stays persistent as long as there is not children page under it")
	public void DA_LOGIN_TC017_Verify_that_user_can_remove_any_main_parent_page_except_Overview_page_successfully_and_the_order_of_pages_stays_persistent_as_long_as_there_is_not_children_page_under_it() {
		ExtentTestManager.startTest("DA_LOGIN_TC017", "Verify that user can remove any main parent page except \\\"Overview\\\" page successfully and the order of pages stays persistent as long as there is not children page under it");
		
		Page parent_page = new Page("ParentPage" + strRandomString,null,"","",true);
		Page child_page = new Page("ChildPage" + strRandomString,parent_page,"","",true);
		Page Overview = new Page("Overview",null,"","",true);
		String page_path = String.format("%s->%s", parent_page.getPageName(),child_page.getPageName());
		String unable_delete_page_error_message = String.format(Constant.UNABLE_DELETE_PAGE_ERROR_MESSAGE, parent_page.getPageName());
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Log in specific repository with valid account");
		MainPage main_page = login_page.login();
		
		LogWrapper.report("Step3:Add a new parent page");
		LogWrapper.report("Step4:Add a children page of newly added page");
		action.addNewPage(parent_page);
		action.addNewPage(child_page);
		
		LogWrapper.report("Step5:Click on parent page");
		main_page.gotoPage(parent_page);
		
		LogWrapper.report("Step6:Click \"Delete\" link");
		main_page.selectGlobalSettingMenu("Delete");
		
		LogWrapper.report("VP:Check confirm message \"Are you sure you want to remove this page?\" appears");
		softAssertion.sortAssertEquals(main_page.getPopupText(), Constant.DELETE_PAGE_WARNING_MESSAGE,"Dashboard Warning message does not appears");
		
		LogWrapper.report("Step8:Click OK button");
		main_page.acceptPopup();
		
		LogWrapper.report("VP:Check warning message \"Can not delete page 'Test' since it has children page(s)\" appears");
		softAssertion.sortAssertEquals(main_page.getPopupText(), unable_delete_page_error_message,"Dashboard Error message does not appears");
		
		LogWrapper.report("Step10:Click OK button");
		main_page.acceptPopup();
		
		LogWrapper.report("Step11:Click on children page");
		main_page.gotoPage(child_page);
		
		LogWrapper.report("Step12:Click \"Delete\" link");
		main_page.selectGlobalSettingMenu("Delete");
		
		LogWrapper.report("VP:Check confirm message \"Are you sure you want to remove this page?\" appears");
		softAssertion.sortAssertEquals(main_page.getPopupText(), Constant.DELETE_PAGE_WARNING_MESSAGE,"Dashboard Warning message does not appears");
		
		LogWrapper.report("Step14:Click OK button");
		main_page.acceptPopup();
		
		LogWrapper.report("VP:Check children page is deleted");
		boolean child_page_exist = main_page.doesPageExist(page_path);
		softAssertion.sortAssertFalse(child_page_exist,"Unable to delete " +page_path);
		
		LogWrapper.report("Step16:Click on  parent page");
		main_page.gotoPage(parent_page);
		
		LogWrapper.report("Step17:Click \"Delete\" link");
		main_page.selectGlobalSettingMenu("Delete");
		
		LogWrapper.report("VP:Check confirm message \"Are you sure you want to remove this page?\" appearse");
		softAssertion.sortAssertEquals(main_page.getPopupText(), Constant.DELETE_PAGE_WARNING_MESSAGE,"Dashboard Warning message does not appears");
		
		LogWrapper.report("Step19:Click OK button");
		main_page.acceptPopup();
		
		LogWrapper.report("VP:Check parent page is deleted");
		boolean parent_page_exist = main_page.doesPageExist(page_path);
		softAssertion.sortAssertFalse(parent_page_exist,"Unable to delete " + parent_page.getPageName());
		
		LogWrapper.report("Step21:Click on \"Overview\" page");
		main_page.gotoPage(Overview);
		
		LogWrapper.report("VP:Check \"Delete\" link disappears");
		boolean delete_link_exist = main_page.doesDeletePageLinkExists();
		softAssertion.sortAssertFalse(delete_link_exist,"Delete link still exist");
		
		softAssertion.sortAssertAll();
	}

}
