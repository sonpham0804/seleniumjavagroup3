package da.mp.test.cases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import base.test.BaseTestWithLogOut;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.Page;
import objects.User;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC022 extends BaseTestWithLogOut{
	Page page1;
	Page page2;
	LogInPage login_page;
	MainPage main_page;
	
	@Test(description = "Verify that user is unable to duplicate the name of sibbling page under the same parent page")

	public void DA_MP_TC022_Verify_that_user_is_unable_to_duplicate_the_name_of_sibbling_page_under_the_same_parent_page() {
		ExtentTestManager.startTest("DA_MP_TC022", "Verify that user is unable to duplicate the name of sibbling page under the same parent page");
		
		User user = new User(Constant.ROOT_USER.getUserName(),Constant.ROOT_USER.getPassword(),"SampleRepository");
		page1 = new Page("Test" + strRandomString,null,"","",false);
		page2 = new Page("Child Test 1" + strRandomString,page1,"","",true);
		Page page3 = new Page(page2.getPageName(),page1,"","",true);
		String expected = page2.getPageName()+" already exists. Please enter a different name.";
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogWrapper.report("Step2:Log in specific repository with valid account\r\n");
		login_page = new LogInPage();
		main_page = login_page.login(user);
		
		LogWrapper.report("Step3:Add a new page");
		action.addNewPage(page1);
		
		LogWrapper.report("Step4:Add a sibling page of new page");
		action.addNewPage(page2);
		
		LogWrapper.report("Step5:Go to Global Setting -> Add page");	
		LogWrapper.report("Step6:Enter Page Name");
		LogWrapper.report("Step7:Click on  Parent Page dropdown list");
		LogWrapper.report("Step8:Select a parent page");
		LogWrapper.report("Step9:Click OK button");
		action.addNewPage(page3);
		
		LogWrapper.report("Step10:Check warning message \"Test child already exist. Please enter a diffrerent name\" appears");
		main_page.getPopupText();
		
		softAssertion.sortAssertEquals(main_page.getPopupText(), expected, "PopupText are not the same");
		
		main_page.acceptPopup();
		main_page.cancelAddNewPage();
		
		
		softAssertion.sortAssertAll();
	}
	@AfterMethod
	public void afterMethodforTC022() {
		main_page.deletePage(page2);
		main_page.deletePage(page1);
	}
}
