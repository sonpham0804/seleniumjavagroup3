package da.mp.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTest;
import extent.report.ExtentTestManager;
import objects.Page;
import pom.LogInPage;
import pom.MainPage;
import popup.NewPagePopup;
import wrapper.LogWrapper;

public class DA_MP_TC023 extends BaseTest{
	
	@Test (description = "Verify that user is able to edit the parent page of the sibbling page successfully")
	public void DA_LOGIN_TC023_Verify_that_user_is_able_to_edit_the_parent_page_of_the_sibbling_page_successfully() {
		ExtentTestManager.startTest("DA_MP_TC023", "Verify that user is able to edit the parent page of the sibbling page successfully");
		Page Overview = new Page("Overview",null,"","",true);
		Page page1 = new Page("Page 1" + strRandomString,Overview,"","",false);
		Page page2 = new Page("Page 2" + strRandomString,page1,"","",false);
		Page page3 = new Page("Page 3" + strRandomString,null,"","",false);
		String page1_path = String.format("%s->%s", "Overview",page1.getPageName());
		String page3_path = String.format("%s->%s", "Overview",page3.getPageName());
		
		LogWrapper.report("Step1: Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2: Login with valid account");
		MainPage main_page = login_page.login();
		
		LogWrapper.report("Step3: Go to Global Setting -> Add page");
		LogWrapper.report("Step4: Enter info into all required fields on New Page dialog");
		action.addNewPage(page1);
		
		LogWrapper.report("Step5: Go to Global Setting -> Add page");
		LogWrapper.report("Step6: Enter info into all required fields on New Page dialog");
		action.addNewPage(page2);
		
		LogWrapper.report("Step7: Go to the first created page");
		main_page.gotoPage(page1);
		
		LogWrapper.report("Step8: Click Edit link");
		main_page.selectGlobalSettingMenu("Edit");
		
		LogWrapper.report("Step9: Enter another name into Page Name field");
		LogWrapper.report("Step10: Click Ok button on Edit Page dialog");
		NewPagePopup edit_page = new NewPagePopup();
 		edit_page.enterPageInfomation(page3);
		
		LogWrapper.report("VP: Observe the current page - User is able to edit the parent page of the sibbling page successfully");
		Boolean page1_exist = main_page.doesPageExist(page1_path);
		softAssertion.sortAssertFalse(page1_exist, page1_path + " still exists");
		
		Boolean page3_exist = main_page.doesPageExist(page3_path);
		softAssertion.sortAssertTrue(page3_exist, page3_path + " is not exists");
		
		softAssertion.sortAssertAll();
	}

}
