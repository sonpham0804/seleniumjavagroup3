package da.mp.test.cases;

import org.testng.annotations.Test;
import base.test.BaseTestWithLogOut;
import extent.report.ExtentTestManager;
import pom.LogInPage;
import pom.MainPage;
import wrapper.LogWrapper;

public class DA_MP_TC011 extends BaseTestWithLogOut{
	
	@Test(description = "DA_MP_TC011: Verify that user is unable to open mor than New Page dialog")
	public void DA_LOGIN_TC011_Verify_that_user_is_unable_to_open_mor_than_New_Page_dialog() {
		ExtentTestManager.startTest("DA_LOGIN_TC011", "Verify that user is unable to open mor than New Page dialog");
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogInPage login_page = new LogInPage();
		
		LogWrapper.report("Step2:Log in with valid account");
		MainPage main_page = login_page.login();
		
		LogWrapper.report("Step3:Go to Global Setting -> Add page");
		main_page.selectGlobalSettingMenu("Add Page");
		
		boolean clickable = main_page.isGlobalSettingMenuClickable();
		LogWrapper.report("VP:User can not to go to Global Setting -> Add page");
		softAssertion.sortAssertFalse(clickable,"Able to select Global Setting Menu");
		
		softAssertion.sortAssertAll();
	}

}
