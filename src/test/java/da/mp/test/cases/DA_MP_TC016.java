package da.mp.test.cases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import base.test.BaseTestWithLogOut;
import constant.Constant;
import extent.report.ExtentTestManager;
import objects.Page;
import objects.User;
import pom.LogInPage;
import pom.MainPage;
import popup.BasePopup;
import wrapper.LogWrapper;

public class DA_MP_TC016 extends BaseTestWithLogOut{
	private Page page1;
	private Page page2;
	private LogInPage login_page;
	private MainPage main_page;
	
	@Test(description = "Verify that user is able to edit the \"Public\" setting of any page successfully")
	public void DA_MP_TC016_Verify_that_user_is_able_to_edit_the_Public_setting_of_any_page_successfully() {
		ExtentTestManager.startTest("DA_MP_TC016", "Verify_that_user_is_able_to_edit_the_\"Public\"_setting_of_any_page_successfully");
		
		User user2 = new User(Constant.TEST_USER1.getUserName(),Constant.TEST_USER1.getPassword());
		page1 = new Page("Test",null,"","",false);
		page2 = new Page("Another Test",null,"","",true);
		Page page1_visible = new Page("Test",null,"","",true);
		Page page2_invisible = new Page("Another Test",null,"","",false);
		
		LogWrapper.report("Step1:Navigate to Dashboard login page");
		LogWrapper.report("Step2:Log in specific repository with valid account\r\n");
		login_page = new LogInPage();
		main_page = login_page.login();
		
		LogWrapper.report("Step3:Go to Global Setting -> Add page\r\n");	
		LogWrapper.report("Step4:Enter Page Name field\r\n");
		LogWrapper.report("Step5:Click OK button\r\n");
		action.addNewPage(page1);
		
		LogWrapper.report("Step6:Go to Global Setting -> Add page\r\n");
		LogWrapper.report("Step7:Enter Page Name field");
		LogWrapper.report("Step8:Check Public checkbox");
		LogWrapper.report("Step9:Click OK button");
		action.addNewPage(page2);
		
		LogWrapper.report("Step10:Click on \"Test\" page");
		main_page.gotoPage(page1);
		
		LogWrapper.report("Step11:Click on \"Edit\" link");
		main_page.selectGlobalSettingMenu("Edit");
		
		LogWrapper.report("Step12:Check \"Edit Page\" pop up window is displayed");
		BasePopup popup = new BasePopup();
		Boolean Edit_page_exist = popup.isPopupExist("Edit Page");	
		softAssertion.sortAssertTrue(Edit_page_exist,"False");
		
		
		LogWrapper.report("Step13:Check Public checkbox");
		LogWrapper.report("Step14:Click OK button");
		action.editPage(page1_visible);
		
		LogWrapper.report("Step15:Click on \"Another Test\" page");
		main_page.gotoPage(page2);
		
		LogWrapper.report("Step16:Click on \"Edit\" link");
		main_page.selectGlobalSettingMenu("Edit");
		
		LogWrapper.report("Step17:Check \"Edit Page\" pop up window is displayed");
		softAssertion.sortAssertTrue(Edit_page_exist,"False");
		
		LogWrapper.report("Step18:Uncheck Public checkbox");
		LogWrapper.report("Step19:Click OK button");
		action.editPage(page2_invisible);
		
		LogWrapper.report("Step20:Click Log out link");
		main_page.logOut();
		
		LogWrapper.report("Step21:Log in with another valid account");
		login_page.login(user2);
		
		LogWrapper.report("Step22:Check \"Test\" Page is visible and can be accessed");
		softAssertion.sortAssertEquals(main_page.getPageIndex(page1.getPageName()),2, "Page does not exist");
		main_page.gotoPage(page1);
		
		LogWrapper.report("Step23:Check \"Another Test\" page is invisible");
		softAssertion.sortAssertEquals(main_page.getPageIndex("Another Test"), 0, "Page exists");
		
		softAssertion.sortAssertAll();
	}
	
	@AfterMethod
	public void afterMethodforTC016() {
		main_page.logOut(main_page.getUserName());
		login_page.login();
		main_page.deletePage(page1);
		main_page.deletePage(page2);
	}
}
