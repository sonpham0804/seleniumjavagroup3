package extent.report;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import base.test.BaseTest;
import wrapper.WebDriverWrapper;

public class TestListener extends BaseTest implements ITestListener {
	
	    @Override
	    public void onStart(ITestContext iTestContext) {
	        iTestContext.setAttribute("WebDriver", WebDriverWrapper.getWebDriver());
	    }
	 
	    @Override
	    public void onFinish(ITestContext iTestContext) {
	        //Do tier down operations for extentreports reporting!
	        ExtentTestManager.endTest();
	        ExtentManager.getReporter().flush();
	    }
	 
	    @Override
	    public void onTestStart(ITestResult iTestResult) {
	    }
	 
	    @Override
	    public void onTestSuccess(ITestResult iTestResult) {
	        //ExtentReports log operation for passed tests.
	        ExtentTestManager.getTest().log(LogStatus.PASS, "Test passed");
	    }
	 
	    @Override
	    public void onTestFailure(ITestResult iTestResult) {
	    	ExtentTestManager.getTest().log(LogStatus.FAIL, iTestResult.getName() + " Test is failed" + iTestResult.getThrowable());
	    }
	 
	    @Override
	    public void onTestSkipped(ITestResult iTestResult) {
	        //ExtentReports log operation for skipped tests.
	        ExtentTestManager.getTest().log(LogStatus.SKIP, iTestResult.getName() + " Test is Skipped" +  iTestResult.getThrowable());
	    }
	 
	    @Override
	    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
	    }
}
