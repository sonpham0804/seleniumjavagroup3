package extent.report;

import com.relevantcodes.extentreports.ExtentReports;
import constant.Constant;

public class ExtentManager {

	private static ExtentReports extent;

	public synchronized static ExtentReports getReporter() {
		if (extent == null) {
			// Set HTML reporting file location
			extent = new ExtentReports(Constant.EXTENT_REPORT_PATH + System.currentTimeMillis() + ".html", true);
		}
		return extent;
	}
}
