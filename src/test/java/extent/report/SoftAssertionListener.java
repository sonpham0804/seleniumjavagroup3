package extent.report;
import java.io.IOException;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.IAssert;
import org.testng.asserts.SoftAssert;
import org.testng.collections.Maps;

import com.relevantcodes.extentreports.LogStatus;

import wrapper.WebDriverWrapper;

public class SoftAssertionListener extends SoftAssert {
	 private final Map<AssertionError, IAssert<?>> m_errors = Maps.newLinkedHashMap();

	 @Override
	 protected void doAssert(IAssert<?> a) {
	  onBeforeAssert(a);

	  try {
	   a.doAssert();
	   onAssertSuccess(a);
	  } catch (AssertionError ex) {
	   onAssertFailure(a, ex);
	   m_errors.put(ex, a);

	      try {
		        WebDriver webDriver = WebDriverWrapper.getWebDriver();
		        String base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) webDriver).
		            getScreenshotAs(OutputType.BASE64);
		            ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot);
		            ExtentTestManager.getTest().log(LogStatus.FAIL, "Test Failed",
		    	            ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
	   } catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	   }
	  } finally {
	   onAfterAssert(a);
	  }
	 }
	 public void assertAll() {
	  if (!m_errors.isEmpty()) {
	   StringBuilder sb = new StringBuilder("The following asserts failed:");   boolean first = true;
	   for (Map.Entry<AssertionError, IAssert<?>> ae : m_errors.entrySet()) {
	    if (first) {
	     first = false;
	    } else {
	     sb.append(",");
	    }
	    sb.append("\n\t");
	    sb.append(ae.getKey().getMessage());
	   }
	   throw new AssertionError(sb.toString());
	  }
	 }
	}
