package actions;

import objects.Page;
import pom.MainPage;
import popup.NewPagePopup;

public class Actions {
	
	public MainPage addNewPage(Page page) {
		MainPage main_page = new MainPage();
		main_page.selectGlobalSettingMenu("Add Page");
		NewPagePopup new_page = new NewPagePopup();
		new_page.enterPageInfomation(page);
		main_page.waitUntilTitle(page.getPageName());
		return new MainPage();
	}

	public MainPage editPage(Page page) {

		MainPage main_page = new MainPage();
		NewPagePopup edit_page = new NewPagePopup();
		edit_page.enterPageInfomation(page);
		main_page.waitUntilTitle(page.getPageName());
		return new MainPage();
	}

	public MainPage editPage(Page old_page, Page new_page) {

		MainPage main_page = new MainPage();
		main_page.gotoPage(old_page);
		main_page.selectGlobalSettingMenu("Edit");
		NewPagePopup edit_page = new NewPagePopup();
		edit_page.enterPageInfomation(new_page);
		main_page.waitUntilTitle(new_page.getPageName());
		return new MainPage();
	}
}
