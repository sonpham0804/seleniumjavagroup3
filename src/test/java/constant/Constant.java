package constant;

import java.io.File;
import objects.User;
public class Constant {
	
	//Constant values
	public static final String REPO = "SampleRepository";
	public static final User ROOT_USER = new User("administrator","");
	public static final User TEST_USER1 = new User("tester","TESTER");
	public static final User TEST_USER2 = new User("specialUser","/`@^&*(+_=;\"',./<?");
	public static final User TEST_USER3 = new User("`~!@$^&()',.","specialCharsUser");
	public static final int SHORT_TIME = 15;
	public static final int LONG_TIME = 60;

	
	//Path
	public static final String CONFIG_BROWSER_PATH = new StringBuilder().append(System.getProperty("user.dir"))
			.append(File.separator).append("src").append(File.separator).append("test")
			.append(File.separator).append("sources").append(File.separator).append("config")
			.append(File.separator).append("%s.json").toString();
	
	public static final String CONFIG_FILE_LOG_PATH = new StringBuilder().append(System.getProperty("user.dir"))
			.append(File.separator).append("src").append(File.separator).append("test")
			.append(File.separator).append("sources").append(File.separator).append("config")
			.append(File.separator).append("logging.properties").toString();
	
	public static final String LOCATOR_JSON_FILE_PATH = new StringBuilder().append(System.getProperty("user.dir"))
			.append(File.separator).append("src").append(File.separator).append("test")
			.append(File.separator).append("sources").append(File.separator).append("locators")
			.append(File.separator).append("%s.json").toString();
	
	public static final String SELENIUM_GRID_FOLDER_PATH = new StringBuilder().append(System.getProperty("user.dir"))
			.append(File.separator).append("src").append(File.separator).append("test")
			.append(File.separator).append("sources").append(File.separator).append("seleniumGrid").toString();
	
	public static final String START_HUB_BAT = new StringBuilder().append(SELENIUM_GRID_FOLDER_PATH)
			.append(File.separator).append("start_hub.bat").toString();
	
	public static final String START_NODE_BAT = new StringBuilder().append(SELENIUM_GRID_FOLDER_PATH)
			.append(File.separator).append("start_node.bat").toString();
	
	public static final String EXTENT_REPORT_PATH = new StringBuilder().append(System.getProperty("user.dir"))
			.append(File.separator).append("src").append(File.separator).append("test")
			.append(File.separator).append("sources").append(File.separator).append("extent.reports")
			.append(File.separator).append("ExtentReportResults").toString();
	
	//Titles
	public static final String MAIN_TITLE = "TestArchitect ™ - Execution Dashboard";
	
	//Messages
	public static final String INVALID_USER_ERROR_MESSAGE = "Username or password is invalid";
	public static final String DELETE_PAGE_WARNING_MESSAGE = "Are you sure you want to remove this page?";	
	public static final String UNABLE_DELETE_PAGE_ERROR_MESSAGE = "Cannot delete page '%s' since it has child page(s).";
	
}
