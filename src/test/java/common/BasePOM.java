package common;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import constant.Constant;
import objects.Locator;
import wrapper.LogWrapper;

public class BasePOM {
	private static HashMap<String, Locator> _locators = null;
	private static final Logger logger = LogWrapper.createLogger(BasePOM.class.getName());

	public BasePOM() {
		getLocatorsByClassName(this.getClass().getName());
	}

	public void getLocatorsByClassName(String className) {
		HashMap<String, Locator> locators = new HashMap<String, Locator>();
		className = className.substring(className.lastIndexOf('.') + 1);

		String filePath = String.format(Constant.LOCATOR_JSON_FILE_PATH, className);

		try {
			String text = new String(Files.readAllBytes(Paths.get(filePath)), StandardCharsets.UTF_8);

			JSONObject jObject = new JSONObject(text);

			JSONArray arrProperties = jObject.names();
			for (int i = 0; i < arrProperties.length(); i++) {
				String propertyName = arrProperties.getString(i);
				JSONObject jsonLocator = jObject.getJSONObject(propertyName);
				Locator locator = new Locator(jsonLocator.getString("type"), jsonLocator.getString("value"));
				locator.setBy(getByLocator(locator.getType(), locator.getValue()));
				locators.put(propertyName, locator);
			}
		} catch (Exception e) {
			logger.info(e.toString());
		}
		_locators = locators;
	}

	protected Locator getLocator(String elementName) {
		return _locators.get(elementName);
	}

	public static By getByLocator(String type, String value) {
		switch (type) {
		case "css":
			return By.cssSelector(value);
		case "id":
			return By.id(value);
		case "link":
			return By.linkText(value);
		case "tagName":
			return By.tagName(value);
		case "name":
			return By.name(value);
		default:
			return By.xpath(value);
		}
	}
}