package wrapper;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.relevantcodes.extentreports.LogStatus;

import constant.Constant;
import extent.report.ExtentTestManager;

public class LogWrapper {
	private static final Logger logger = Logger.getLogger(LogWrapper.class);
	static {
		PropertyConfigurator.configure(Constant.CONFIG_FILE_LOG_PATH);
	}

	public static final Logger createLogger(String className) {
		return Logger.getLogger(className);
	}

	public static void report(String message) {
		logger.info(message);
		ExtentTestManager.getTest().log(LogStatus.INFO, message);
	}

}
