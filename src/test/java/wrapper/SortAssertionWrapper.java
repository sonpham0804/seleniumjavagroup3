package wrapper;

import org.apache.log4j.Logger;
import extent.report.SoftAssertionListener;

public class SortAssertionWrapper {
	private static final Logger logger = LogWrapper.createLogger(SortAssertionWrapper.class.getName());
	private SoftAssertionListener softAssertion;
	
	public SortAssertionWrapper() {
		this.softAssertion = new SoftAssertionListener();
	}
	
	
	public void sortAssertEquals(String actual, String expected, String msg) {
		this.softAssertion.assertEquals(actual, expected, msg);
		logger.info(String.format("Recorded: %s", actual));
		logger.info(String.format("Expected: %s", expected));
	}

	public void sortAssertEquals(int actual, int expected, String msg) {
		this.softAssertion.assertEquals(actual, expected, msg);
		logger.info(String.format("Recorded: %s", actual));
		logger.info(String.format("Expected: %s", expected));
	}

	public void sortAssertTrue(Boolean condition, String msg) {
		this.softAssertion.assertTrue(condition, msg);
		logger.info(String.format("Recorded: %s", condition));
		logger.info("Expected: true");
	}

	public void sortAssertFalse(Boolean condition, String msg) {
		this.softAssertion.assertFalse(condition, msg);
		logger.info(String.format("Recorded: %s", condition));
		logger.info("Expected: false");
	}

	public void sortAssertAll() {
		this.softAssertion.assertAll();
	}
}
