package wrapper;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

import constant.Constant;

public class DriverConfig {

	private static HashMap<String, String> _properties = null;

	public static void getDriverConfig(String type) {
		HashMap<String, String> properties = new HashMap<String, String>();

		String filePath = String.format(Constant.CONFIG_BROWSER_PATH, type);

		try {
			String text = new String(Files.readAllBytes(Paths.get(filePath)), StandardCharsets.UTF_8);
			JSONObject jObject = new JSONObject(text);
			JSONArray arrProperties = jObject.names();
			for (int i = 0; i < arrProperties.length(); i++) {
				String propertyName = arrProperties.getString(i);
				JSONObject jsonOption = jObject.getJSONObject(propertyName);
				properties.put(propertyName, jsonOption.getString("value"));
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		_properties = properties;
	}

	public static String getProperty(String propertyName) {
		if (_properties.containsKey(propertyName))
			return _properties.get(propertyName);
		return "";
	}

	public static final String KEY_EXEC_PATH = "capability.executable_path";
	public static final String KEY_REMOTE_URL = "capability.remoteserver";
	public static final String KEY_STARTUP_URL = "capability.startup_url";
}
