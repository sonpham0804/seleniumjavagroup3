package wrapper;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import constant.Constant;

public class WebElementWrapper {
	private static final Logger logger = LogWrapper.createLogger(WebElementWrapper.class.getName());

	private By _locator;

	protected By getLocator() {
		return this._locator;
	}

	public WebElementWrapper(String xpath) {
		this._locator = By.xpath(xpath);

	}

	public WebElementWrapper(String dynamicxpath, String text) {
		this._locator = By.xpath(String.format(dynamicxpath, text));
	}

	public List<WebElement> getElements() {
		List<WebElement> elements = null;
		try {
			elements = WebDriverWrapper.getWebDriver().findElements(this._locator);
		} catch (Exception error) {
			logger.error(String.format("Has error with control '%s': %s", this.getLocator(), error.getMessage()));
		}
		return elements;
	}

	public WebElement getElement() {
		WebElement element = null;
		try {
			element = WebDriverWrapper.getWebDriver().findElement(this._locator);
		} catch (Exception error) {
			logger.error(String.format("Has error with control '%s': %s", this.getLocator(), error.getMessage()));
		}
		return element;
	}

	public void waitForControlPresent(int timeOutInSeconds) {
		try {
			logger.info(String.format("Wait for element %s is present on the DOM", this.getLocator()));
			WebDriverWait wait = new WebDriverWait(WebDriverWrapper.getWebDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.presenceOfElementLocated(this.getLocator()));
		} catch (Exception error) {
			logger.error(String.format("Has error with control '%s': %s", this.getLocator(), error.getMessage()));
		}
	}

	public void waitForControlDispay(int timeOutInSeconds) {
		try {
			logger.info(String.format("Wait for element %s to be displayed", this.getLocator()));
			WebDriverWait wait = new WebDriverWait(WebDriverWrapper.getWebDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.visibilityOfElementLocated(this.getLocator()));
		} catch (Exception error) {
			logger.error(String.format("Has error with control '%s': %s", this.getLocator(), error.getMessage()));
		}
	}

	public void waitForControlDisappear(int timeOutInSeconds) {
		try {
			logger.info(String.format("Wait for element %s to be disappeared", this.getLocator()));
			WebDriverWait wait = new WebDriverWait(WebDriverWrapper.getWebDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(this.getLocator()));
		} catch (Exception error) {
			logger.info(String.format("Element %s still displayed", this.getLocator()));
		}
	}

	public void waitForControlNotExist(int timeOutInSeconds) {
		logger.info(String.format("Wait for element %s not exist", this.getLocator()));
		Boolean result = true;
		while (timeOutInSeconds > 0) {
			try {
				WebDriverWait wait = new WebDriverWait(WebDriverWrapper.getWebDriver(), 1);
				wait.until(ExpectedConditions.presenceOfElementLocated(this.getLocator()));
				result = true;
				timeOutInSeconds = timeOutInSeconds - 1;
				if (timeOutInSeconds == 0) {
					result = false;
					break;
				}

			} catch (Exception error) {
				logger.info(String.format("Element %s is not exist", this.getLocator()));
				result = false;
				break;
			}
		}
		if (result == true)
			logger.info(String.format("Element %s still exist", this.getLocator()));
	}

	public void waitForAllControlDispay(int timeOutInSeconds) {
		try {
			logger.info(String.format("Wait for all elements %s to be displayed", this.getLocator()));
			WebDriverWait wait = new WebDriverWait(WebDriverWrapper.getWebDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(this.getLocator()));
		} catch (Exception error) {
			logger.error(String.format("Has error with control '%s': %s", this.getLocator(), error.getMessage()));
		}
	}

	public void waitFoControlNotDispay(int timeOutInSeconds) {
		try {
			logger.info(String.format("Wait for element %s to be not displayed", this.getLocator()));
			WebDriverWait wait = new WebDriverWait(WebDriverWrapper.getWebDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(this.getLocator()));
		} catch (Exception error) {
			logger.error(String.format("Has error with control '%s': %s", this.getLocator(), error.getMessage()));
		}
	}

	public void waitForControlClickable(int timeOutInSeconds) {
		try {
			logger.info(String.format("Wait for element %s to be clickable", this.getLocator()));
			WebDriverWait wait = new WebDriverWait(WebDriverWrapper.getWebDriver(), timeOutInSeconds);
			wait.until(ExpectedConditions.elementToBeClickable(this.getLocator()));
		} catch (Exception error) {
			logger.error(String.format("Has error with control '%s': %s", this.getLocator(), error.getMessage()));
		}
	}

	public boolean isClickable(int timeout) {
		boolean result = false;
		try {
			this.waitForControlClickable(timeout);
			logger.info(String.format("Element %s is clickable", this.getLocator()));
			result = true;
		} catch (Exception e) {
			logger.info(String.format("Element %s is unclickable", this.getLocator()));
		}
		return result;
	}

	public boolean isDisplayed() {
		this.waitForControlDispay(5);
		boolean result = false;
		try {
			result = getElement().isDisplayed();
			logger.info(String.format("Element %s is display", this.getLocator()));
			result = true;
		} catch (Exception e) {
			logger.info(String.format("Element %s is not display", this.getLocator()));
			return false;
		}
		return result;
	}

	public boolean isExists() {
		boolean result = false;
		try {
			WebDriverWrapper.getWebDriver().findElement(this._locator);
			logger.info(String.format("Element %s is exists", this.getLocator()));
			result = true;
		} catch (Exception error) {
			logger.info(String.format("Element %s is not exists", this.getLocator()));
			return false;
		}
		return result;
	}

	public void click() {
		try {

			this.waitForControlClickable(Constant.SHORT_TIME);
			logger.info(String.format("Click on element %s", this.getLocator()));
			this.getElement().click();
		} catch (StaleElementReferenceException e) {
			logger.info(String.format("Unable to click on element %s!", this.getLocator()));
		}
	}

	public void selectByName(String item) {
		try {
			this.waitForControlPresent(Constant.SHORT_TIME);
			logger.info(String.format("Select item: %s from element: %s", item, this.getLocator()));
			Select select = new Select(this.getElement());
			select.selectByVisibleText(item);
		} catch (StaleElementReferenceException e) {
			logger.info(String.format("Unable to  select item: %s from element: %s", item, this.getLocator()));
		}
	}

	public void selectByNameContaining(String item) {
		try {
			this.waitForControlPresent(Constant.SHORT_TIME);
			logger.info(String.format("Select item: %s from element: %s", item, this.getLocator()));
			this.click();
			String option_xpath = "//option[contains(text(),'%s')]";
			WebElementWrapper ele = new WebElementWrapper(String.format(option_xpath, item));
			ele.click();
		} catch (StaleElementReferenceException e) {
			logger.info(String.format("Unable to  select item: %s from element: %s", item, this.getLocator()));
		}
	}

	public void enter(String str) {
		try {
			this.waitForControlPresent(Constant.SHORT_TIME);
			logger.info(String.format("Enter: '%s' into element: %s", str, this.getLocator()));
			this.getElement().clear();
			this.getElement().sendKeys(str);
		} catch (StaleElementReferenceException e) {
			logger.info(String.format("Unable to enter: '%s' into element: %s", str, this.getLocator()));
		}
	}

	public void moveMouse() {
		try {

			this.waitForControlClickable(Constant.LONG_TIME);
			Actions builder = new Actions(WebDriverWrapper.getWebDriver());
			Action actMoveMouse = builder.moveToElement(this.getElement()).build();
			actMoveMouse.perform();
			// Add sleep 3 seconds to make sure the next item to move mouse is stable
		} catch (Exception e) {
			logger.info("Cannot move mouse due to exception " + e);
		}
	}

	public String getText() {
		logger.info(String.format("Get text from element: '%s'", this.getElement()));
		String text = null;
		try {
			text = this.getElement().getText();
		} catch (Exception e) {
			logger.info("Cannot get text" + e.getMessage());
		}
		return text;
	}

	public void setCheckbox(boolean status) {
		try {
			this.waitForControlPresent(Constant.SHORT_TIME);
			if (status != this.getElement().isSelected()) {
				this.getElement().click();
			}
		} catch (Exception e) {
			logger.info("Unable to set check box:\n " + e);
		}
	}
}
