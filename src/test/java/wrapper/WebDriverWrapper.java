package wrapper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.net.URL;
import org.apache.log4j.Logger;
import constant.Constant;
import wrapper.DriverConfig;

public class WebDriverWrapper {
	private static final Logger logger = LogWrapper.createLogger(WebDriverWrapper.class.getName());
	private static ThreadLocal<WebDriver> drivers = new ThreadLocal<WebDriver>();

	public static void startDriver(String type, String browser) {

		DriverConfig.getDriverConfig(browser);

		if (type.equals("local")) {

			if (browser.equals("chrome")) {
				System.setProperty("webdriver.chrome.driver", DriverConfig.getProperty(DriverConfig.KEY_EXEC_PATH));

				ChromeOptions options = new ChromeOptions();
				options.addArguments("start-maximized");

				setDriver(new ChromeDriver(options));

			} else if (type.equals("firefox")) {
				// TODO Auto-generated catch block
			}
		} else if (type.equals("remote")) {
			if (browser.equals("chrome")) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("start-maximized");

				try {
					WebDriver driver = new RemoteWebDriver(
							new URL(DriverConfig.getProperty(DriverConfig.KEY_REMOTE_URL)), options);
					setDriver(driver);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (type.equals("firefox")) {
				// TODO Auto-generated catch block
			}

		}
	}

	public static WebDriver getWebDriver() {
		return drivers.get();
	}

	public static void setDriver(WebDriver driver) {
		drivers.set(driver);
	}

	public static void close() {
		logger.info("Close browser");
		if (getWebDriver() != null)
			getWebDriver().close();
	}

	public static void quit() {
		logger.info("Quit browser");
		if (getWebDriver() != null)
			getWebDriver().quit();
	}

	public static String getCurrentUrl() {
		logger.info("Get current Url");
		return getWebDriver().getCurrentUrl();
	}

	public static String getTitle() {
		logger.info("Get title current page");
		return getWebDriver().getTitle();
	}

	public static void waitUntilTitle(String expected_title, int timeout) {
		logger.info("Wait until title");
		try {
			WebDriverWait wait = new WebDriverWait(getWebDriver(), timeout);
			wait.until(ExpectedConditions.titleContains(expected_title));
		} catch (Exception e) {
			logger.info("Error: " + e);
		}
	}

	public static void navigateTo(String url) {
		try {
			logger.info("Start Navigate to " + url);
			getWebDriver().navigate().to(url);
			logger.info("Finished navigate to " + url);
		} catch (Exception e) {
			logger.info("Error: Cannot navigate to " + url + " due to " + e);
		}
	}

	public static void maximizeWindow() {
		logger.info("Maximize window");
		getWebDriver().manage().window().maximize();
	}

	public static void switchTo(String popup) {
		logger.info("Switch to the popup");
		getWebDriver().switchTo().window(popup);
	}

	public static void waitForPopupDisplay() {
		logger.info("Wait for Popup Display");
		try {
			WebDriverWait wait = new WebDriverWait(getWebDriver(), Constant.SHORT_TIME);
			wait.until(ExpectedConditions.alertIsPresent());
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

	public static void waitForPopupDisplay(int timeout) {
		logger.info("Wait for Popup Display");
		try {
			WebDriverWait wait = new WebDriverWait(getWebDriver(), timeout);
			wait.until(ExpectedConditions.alertIsPresent());
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

	public static void switchToAlert() {
		logger.info("Switch to displayed alert");
		waitForPopupDisplay();
		getWebDriver().switchTo().alert();
	}

	public static String getAlertMessage() {
		logger.info("Get the alert message");
		String alr_text = getWebDriver().switchTo().alert().getText();
		return alr_text;
	}

	public static void dissmissAlert() {
		logger.info("dissmiss the alert dialog");
		getWebDriver().switchTo().alert().dismiss();
		waitForPopupDisappear(Constant.SHORT_TIME);
	}

	public static void acceptAlert() {
		logger.info("Accept the alert dialog");
		getWebDriver().switchTo().alert().accept();
	}

	public static void waitForPopupDisappear(int timeout) {
		logger.info("Wait for Popup Disappear");
		boolean result = true;
		while (timeout > 0) {
			try {
				WebDriverWait wait = new WebDriverWait(getWebDriver(), 2);
				wait.until(ExpectedConditions.alertIsPresent());
				result = true;
				timeout = timeout - 2;
			} catch (Exception e) {
				result = false;
				break;
			}
			if (timeout < 0)
				break;
		}

		if (result == false)
			logger.info("Popup is disappeared");
		else
			logger.info("Popup still displays");
	}
}
