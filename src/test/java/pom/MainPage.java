package pom;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import constant.Constant;
import objects.Page;
import popup.NewPagePopup;
import org.openqa.selenium.By;
import wrapper.LogWrapper;
import wrapper.WebDriverWrapper;
import wrapper.WebElementWrapper;

public class MainPage extends GeneralPage {
	private static final Logger logger = LogWrapper.createLogger(MainPage.class.getName());
	private String txtDelete = "Delete";
	private String txtAdministrator = "administrator";
	private String txtLogout = "Logout";
	
	public MainPage() {
	}

	public MainPage(String usr) {
		this.getMainMenuCombobox(usr).waitForControlPresent(constant.Constant.SHORT_TIME);
	}

	// Elements
	public WebElementWrapper getMainMenuCombobox(String usr) {
		return new WebElementWrapper(getLocator("lnkMainMenuDynamicXpath").getValue(), usr);
	}

	public WebElementWrapper getbtnUserName(String usr) {
		return new WebElementWrapper(getLocator("btnUserName").getValue());
	}

	public WebElementWrapper getGlobalSettingListItem(String pg) {
		return new WebElementWrapper(getLocator("lnkGlobalSettingDynamicXpath").getValue(), pg);
	}

	public WebElementWrapper getGlobalSettingButton() {
		return new WebElementWrapper(getLocator("btnGlobalSetting").getValue());
	}

	public WebElementWrapper getRepositoryListItem(String usr) {
		return new WebElementWrapper(getLocator("lnkListRepositoryDynamicXpath").getValue(), usr);
	}

	WebElementWrapper spnRepositoryName = new WebElementWrapper(getLocator("spnRepositoryNameXpath").getValue());
	WebElementWrapper ulColumns = new WebElementWrapper(getLocator("ulColumnsXpath").getValue());

	// Methods
	public MainPage chooseRepository(String repoName) {
		this.spnRepositoryName.moveMouse();
		getRepositoryListItem(repoName).click();
		return new MainPage();
	}

	public int countColumns() {
		return ulColumns.getElements().size();
	}

	public boolean doesDeletePageLinkExists() {
		boolean result = false;
		try {
			getGlobalSettingListItem(txtDelete).getElement();
			result = true;
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

	public String getUserName() {
		return new WebElementWrapper("//a[@href='#Welcome']").getText();
	}

	public void selectGlobalSettingMenu(String itemName) {
		this.getGlobalSettingButton().moveMouse();
		getGlobalSettingListItem(itemName).click();
	}

	public boolean isGlobalSettingMenuClickable() {
		return this.getGlobalSettingButton().isClickable(Constant.SHORT_TIME);
	}

	public LogInPage logOut() {
		getMainMenuCombobox(txtAdministrator).click();
		getMainMenuCombobox(txtLogout).click();
		return new LogInPage();
	}

	public LogInPage logOut(String userName) {
		getMainMenuCombobox(userName).click();
		getMainMenuCombobox(txtLogout).click();
		return new LogInPage();
	}

	public String getCurrentRepositoryName() {
		return this.spnRepositoryName.getText();
	}

	public void deletePage(Page page) {
		this.gotoPage(page).selectGlobalSettingMenu(txtDelete);
		this.acceptPopup();
		this.waitForPageDeleted(page.getPageName(), Constant.SHORT_TIME);
	}

	public void cancelAddNewPage() {
		NewPagePopup cancel_page = new NewPagePopup();
		cancel_page.cancelNewPage();
	}

	public ArrayList<String> getAllParentPageName() {
		ArrayList<String> allPage_name = new ArrayList<String>();
		String page_menu = "//a[contains(@href,'TADashboard')]";
		String page_name = "";
		int count = WebDriverWrapper.getWebDriver().findElements(By.xpath(page_menu)).size();

		for (int i = 1; i <= count; i++) {

			String dynamic_element = "(" + page_menu + ")" + "[" + i + "]";

			page_name = (WebDriverWrapper.getWebDriver().findElement(By.xpath(dynamic_element)).getText());
			if (page_name != "") {
				allPage_name.add(page_name);
			}
		}

		return allPage_name;
	}

	public MainPage gotoPage(Page page) {
		// Get node list (node1->node2->....)
		String[] namelist = page.getPath().split("->");
		String new_xpath = "";

		if (namelist.length == 0) {
			// if: path has no node -> nothing happens
			logger.info("Incorrect page path!");

		} else if (namelist.length == 1) {
			// if: path has only 1 node -> click
			// Hande page name, ex Page&nsps;1...
			new_xpath = handlePageNameXpath(namelist[0]);
			new WebElementWrapper(new_xpath).click();
		} else

			for (int i = 0; i < namelist.length; i++) {

				// Hande page name, ex Page&nsps;1...
				String new_node_xpath = handlePageNameXpath(namelist[i]);

				// Move mouse to each node
				new_xpath = new_xpath + new_node_xpath;
				new WebElementWrapper(new_xpath).moveMouse();

				// Handle xpath
				if (i != namelist.length - 1)
					new_xpath = new_xpath + "//parent::li";

				// Click on last node
				if (i == namelist.length - 1) {
					new WebElementWrapper(new_xpath).click();
					// Wait for page opened
					this.waitUntilTitle(namelist[i]);
				}
			}
		return new MainPage();
	}

	public boolean doesPageExist(String path) {
		boolean result = false;
		String[] namelist = path.split("->");
		String new_xpath = "";
		if (namelist.length == 1) {
			new_xpath = handlePageNameXpath(namelist[0]);
			result = new WebElementWrapper(new_xpath).isExists();
		} else

			for (int i = 0; i < namelist.length; i++) {
				String new_node_xpath = handlePageNameXpath(namelist[i]);
				new_xpath = new_xpath + new_node_xpath;
				if (i != namelist.length - 1)
					new_xpath = new_xpath + "//parent::li";
				if (i == namelist.length - 1)
					result = new WebElementWrapper(new_xpath).isExists();
			}
		return result;
	}

	public void waitForPageDeleted(String path, int timeout) {
		String[] namelist = path.split("->");
		String new_xpath = "";
		if (namelist.length == 1) {
			new_xpath = handlePageNameXpath(namelist[0]);
		} else

			for (int i = 0; i < namelist.length; i++) {
				String new_node_xpath = handlePageNameXpath(namelist[i]);
				new_xpath = new_xpath + new_node_xpath;
				if (i != namelist.length - 1)
					new_xpath = new_xpath + "//parent::li";
			}
		new WebElementWrapper(new_xpath).waitForControlNotExist(timeout);
	}

	public String handlePageNameXpath(String pagename) {

		String dynamic_page = "//a[contains(@href,'TA')";
		String[] namelist = pagename.split(" ");
		if (namelist.length == 1) {
			dynamic_page = dynamic_page + "and text() = '" + pagename + "']";
		} else
			for (int i = 0; i < namelist.length; i++) {
				dynamic_page = dynamic_page + "and contains(text(),'" + namelist[i] + "')";
				if (i == namelist.length - 1) {
					dynamic_page += "]";
				}
			}
		return dynamic_page;

	}

	public int getPageIndex(String string) {

		int page_index = 0;
		int list_index = this.getAllParentPageName().size();

		for (int i = 1; i < list_index; i++) {

			if (string.contentEquals(this.getAllParentPageName().get(i))) {

				page_index = i + 1;
			}
		}
		return page_index;
	}

}