package pom;

import constant.Constant;
import objects.User;
import wrapper.WebElementWrapper;

public class LogInPage extends GeneralPage {
	public LogInPage() {
	}

	// Locators
	WebElementWrapper txtUserName = new WebElementWrapper(getLocator("txtUserName").getValue());
	WebElementWrapper btnLogin = new WebElementWrapper(getLocator("btnLogin").getValue());
	WebElementWrapper txtPassword = new WebElementWrapper(getLocator("txtPassword").getValue());
	WebElementWrapper cmbRepo = new WebElementWrapper(getLocator("cmbRepo").getValue());

	// Method
	public MainPage login() {
		this.cmbRepo.selectByName(Constant.ROOT_USER.getRepo());
		this.txtUserName.enter(Constant.ROOT_USER.getUserName());
		this.txtPassword.enter(Constant.ROOT_USER.getPassword());
		this.btnLogin.click();
		return new MainPage(Constant.ROOT_USER.getUserName());
	}

	public MainPage login(User user) {
		this.cmbRepo.selectByName(user.getRepo());
		this.txtUserName.enter(user.getUserName());
		this.txtPassword.enter(user.getPassword());
		this.btnLogin.click();
		return new MainPage(user.getUserName());
	}

	public LogInPage failureLogin(User user) {
		this.cmbRepo.selectByName(user.getRepo());
		this.txtUserName.enter(user.getUserName());
		this.txtPassword.enter(user.getPassword());
		this.btnLogin.click();
		return new LogInPage();
	}

}
