package pom;

import common.BasePOM;
import constant.Constant;
import wrapper.WebDriverWrapper;

public class GeneralPage extends BasePOM {

	public GeneralPage() {
	}

	// Methods
	public String getTitle() {
		return WebDriverWrapper.getTitle();
	}

	public String getPopupText() {
		WebDriverWrapper.waitForPopupDisplay();
		return WebDriverWrapper.getAlertMessage();
	}

	public GeneralPage acceptPopup() {
		WebDriverWrapper.waitForPopupDisplay();
		WebDriverWrapper.acceptAlert();
		return new GeneralPage();
	}

	public void waitUntilTitle(String expected_title, int timeout) {
		WebDriverWrapper.waitUntilTitle(expected_title, timeout);
	}

	public void waitUntilTitle(String expected_title) {
		WebDriverWrapper.waitUntilTitle(expected_title, Constant.SHORT_TIME);
	}
}
