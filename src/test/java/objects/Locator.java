package objects;

import org.openqa.selenium.By;

public class Locator {
	private String _value;
	private String _type;
	private By _by;

	public Locator(String type, String value) {
		this._value = value;
		this._type = type;
	}

	public Locator(String type, String value, By by) {
		this._value = value;
		this._by = by;
		this._type = type;
	}

	public By getBy() {
		return _by;
	}

	public void setBy(By by) {
		this._by = by;
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		this._value = value;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		this._type = type;
	}
}
