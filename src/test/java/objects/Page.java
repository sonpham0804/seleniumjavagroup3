package objects;

public class Page {

	private String pageName;
	private Page parentPage;
	private String numberOfColumns;
	private String displayAfterPage;
	private boolean publicPage;
	private String path = "";

	public Page(String pageName, Page parentPage, String numberOfColumns, String displayAfterPage,
			boolean publicPage) {
		this.pageName = pageName;
		this.parentPage = parentPage;
		this.numberOfColumns = numberOfColumns;
		this.displayAfterPage = displayAfterPage;
		this.publicPage = publicPage;
		createPath();
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pagename) {
		this.pageName = pagename;
	}

	public Page getParentPage() {
		return parentPage;
	}

	public void setParentPage(Page parentpage) {
		this.parentPage = parentpage;
	}

	public String getNumberOfColumns() {
		return numberOfColumns;
	}

	public void setNumberOfColumns(String numberofcolumns) {
		this.numberOfColumns = numberofcolumns;
	}

	public String getDisplayAfterPage() {
		return displayAfterPage;
	}

	public void setDisplayAfterPage(String displayafterpage) {
		this.displayAfterPage = displayafterpage;
	}

	public boolean isPublicPage() {
		return publicPage;
	}

	public void setPublicPage(boolean publicpage) {
		this.publicPage = publicpage;
	}
	
	private void createPath() {
		path = this.getPageName() + path;
		if (parentPage != null) {
			path = "->" + path;
			createPath(parentPage);
		}
	}
	
	private void createPath(Page page) {
		path = page.getPageName() + path;
		if (page.getParentPage() != null) {
			path = "->" + path;
			createPath(page.getParentPage());
		}
	}
	
	public String getPath() {
		return path;
	}
}
