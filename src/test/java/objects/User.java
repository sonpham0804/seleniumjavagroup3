package objects;

import constant.Constant;

public class User {
	private String _userName;
	private String _password;
	private String _repo;

	public User() {
	}

	public User(String userName, String password, String repo) {
		this._userName = userName;
		this._password = password;
		this._repo = repo;
	}

	public User(String userName, String password) {
		this._userName = userName;
		this._password = password;
		this._repo = Constant.REPO;
	}

	public String getUserName() {
		return this._userName;
	}

	public String getRepo() {
		return this._repo;
	}

	public String getPassword() {
		return this._password;
	}

	public void setUserName(String userName) {
		this._userName = userName;
	}

	public void setRepo(String repo) {
		this._repo = repo;
	}

	public void setPassword(String password) {
		this._password = password;
	}
}
