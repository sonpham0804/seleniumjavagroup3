package popup;

import common.BasePOM;
import wrapper.WebElementWrapper;

public class BasePopup extends BasePOM {

	// Elements
	public WebElementWrapper getTitleLabel(String str) {
		return new WebElementWrapper(getLocator("lblTitleDynamic").getValue(), str);
	}

	// Methods
	public boolean isPopupExist(String popupName) {
		return getTitleLabel(popupName).isDisplayed();
	}

	public void waitForPopupNotExists(String popupName, int timeout) {
		getTitleLabel(popupName).waitForControlNotExist(timeout);
	}
}