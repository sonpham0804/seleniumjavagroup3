package popup;

import objects.Page;
import popup.BasePopup;
import wrapper.WebElementWrapper;

public class NewPagePopup extends BasePopup {
	public NewPagePopup() {
	}

	// Elements
	WebElementWrapper txtNewPage = new WebElementWrapper(getLocator("txtNewPage").getValue());
	WebElementWrapper btnOK = new WebElementWrapper(getLocator("btnOK").getValue());
	WebElementWrapper cmbDisplayAfter = new WebElementWrapper(getLocator("cmbDisplayAfter").getValue());
	WebElementWrapper cmbParent = new WebElementWrapper(getLocator("cmbParent").getValue());
	WebElementWrapper ckbPublic = new WebElementWrapper(getLocator("ckbPublic").getValue());
	WebElementWrapper cmbColumns = new WebElementWrapper(getLocator("cmbColumns").getValue());
	WebElementWrapper btnCancel = new WebElementWrapper(getLocator("btnCancel").getValue());

	// Methods
	public void enterPageInfomation(Page newpage) {

		if (newpage.getPageName() != "") {

			this.txtNewPage.enter(newpage.getPageName());
		}

		if (newpage.getParentPage() != null) {
			this.cmbParent.selectByNameContaining(newpage.getParentPage().getPageName());
		}

		if (newpage.getNumberOfColumns() != "") {
			this.cmbColumns.selectByName(newpage.getNumberOfColumns());
		}

		if (newpage.getDisplayAfterPage() != "") {
			this.cmbDisplayAfter.selectByName(newpage.getDisplayAfterPage());
		}

		this.ckbPublic.setCheckbox(newpage.isPublicPage());

		this.btnOK.click();
	}

	public void cancelNewPage() {
		this.btnCancel.click();
	}
}
